﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Quiz.Models;

namespace Quiz.Controllers
{
    /// <summary>
    /// Classe que controlara la persitencia del Modelo Question.cs
    /// </summary>
    public class QuestionController : Controller
    {
        /// <summary>
        /// Lista todos los objetos Question recogidos en la base de datos y los devuelvo como Modelo a su vista
        /// </summary>
        /// <returns>View</returns>
        
        public IActionResult List()
        {
            List<Question> questions=new List<Question>();
            
            //TODO Obtener Array con todos los objetos Question de la base de datos
            
            return View(questions);
        }

        /// <summary>
        /// Recoje de la base de datos el objeto Question utilizando la id pasada por parametro y lo devuelvo como modelo
        /// de la vista
        /// </summary>
        /// <param name="id">ID del Objeto Question a recuperar</param>
        /// <returns>View</returns>
        
        public IActionResult Show(int id)
        {
            Question question=new Question();
            
            //TODO Recuperar la question de la Base de datos
            
            return View(question);
        }

        
        /// <summary>
        /// Devuelve la vista del metodo pasandole como modelo el objeto obtenido en la base de datos igual al id del
        /// parametro,la vista sera un formulario que se llenara con las propiedades del objeto. 
        /// </summary>
        ///  <param name="id">Id del objeto question a editar</param>
        /// <returns>View</returns>
        
        public IActionResult Edit(int id)
        {
            Question question=new Question();
            
            //TODO Recuperar el objeto Question de la base de datos y pasarselo como modelo a la vista

            return View(question);
        }
        
        /// <summary>
        /// Accion que Updatea el objeto Question recibido por parametro en el cuerpo de la peticion
        /// en la base de datos.
        /// </summary>
        /// <param name="question">Objeto Question creado a partir del modelo del formulario</param>
        /// <returns>RedirectToAction hacia la Accion List</returns>
        
        [HttpPost]
        public IActionResult Edit(Question question)
        {

            //TODO Persist Objeto question en la base de datos 

            return RedirectToAction("List");
        }
        
        
        
        /// <summary>
        /// Accion que borra de la base de datos el objeto question igual al id pasado por parametro 
        /// </summary>
        /// <param name="id">id del objeto a borrar</param>
        /// <returns>RedirectToAction hacia la Accion List</returns>
        
        public IActionResult Erase(int id)
        {


            return RedirectToAction("List");
        }
        
        /// <summary>
        /// Accion que devolvera una vista tipo formulario para crear un nuevo objeto Question
        /// </summary>
        /// <returns>View</returns>
        
        public IActionResult Add()
        {
            return View();
        }
        
    
        
       
        /// <summary>
        /// Accion que persiste un nuevo objeto Question en la base de datos
        /// </summary>
        /// <param name="question">Objeto question a persistir recibido desde el cuerpo de la peticion</param>
        /// <returns>RedirectToAction hacia la Accion List</returns>
        
        
        [HttpPost]
        public IActionResult Add(Question question)
        {
            
            //TODO Persist Objeto Question en base de datos
            return RedirectToAction("List");
        }

       
    }
}