namespace Quiz.Models
{
    public enum Options {A=0,B=1,C=2,D=3}
    public enum Categorys {Geografia=0,ArteyLiteratura=1,Historia=2,Entretenimiento=3,Ciencias=4,Deportes=5}
    public class Question
    {
        private int Id { get; set; }
        private string QuestionString { get; set; }
        private string[] Answers{ get; set; }
        private Options Option { get; set; }
        private Categorys Category {get; set;}

        public Question(int id,string question,string[]answers,Options option,Categorys category)
        {
            Id = id;
            QuestionString = question;
            Answers = answers;
            Option = option;
            Category = category;
        }


        public Question()
        {
            
        }
        
       

       
    }
}