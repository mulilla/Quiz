using System.Collections.Generic;

namespace Quiz.Models
{
    public class Data
    {
        public Dictionary<int,Question> mapQuiz { get; set; }


        public Data()
        {
            
            mapQuiz.Add(1,new Question(1,"¿Cuál es el lugar más frío de la tierra?",new string[]{"Islandia","Antartida","Artico","Groenlandia"},Options.B,Categorys.Geografia));
            mapQuiz.Add(2,new Question(2,"¿Cómo se llama la capital de Mongolia?",new string[]{"Ulan Bator","Mörön","Jarjorin","Sujbaatar"},Options.A,Categorys.Geografia));
            mapQuiz.Add(3,new Question(3,"¿Quién pintó “la última cena”?",new string[]{"Donatello","Miguel Ángel","Sandro Botticelli","Leonardo da Vinci"},Options.D,Categorys.ArteyLiteratura));
            mapQuiz.Add(4,new Question(4,"¿Quién es el padre del psicoanálisis?",new string[]{"Sigmund Freud","Claudio Naranjo","Nuria Povedano AKA BataMaster","George Gurdjieff"},Options.A,Categorys.Ciencias));
            
        }
           
    }
}