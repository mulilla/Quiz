# Net.Quiz

![triv01](/uploads/a9d289803a9897a6b1aecca9e3b54ccb/triv01.jpg)

**Net.Quiz**, una sencilla aplicación desarrollada en [dotNet core MVC](https://dotnet.github.io/).


## Descripción Del Proyecto
*  La aplicación es un juego de preguntas por turnos con 4 opciones validas
*  Las preguntas están clasificadas por categorías como el juego del *Trivial*
*  De momento solo sera un juego interactivo de preguntas, en un futuro funcionara como el juego del Trivial (Completando "Quesitos..etc.").
* ~~Texto tachado again~~

## Requisitos Tecnicos del Proyecto

1. **Dotnet Core**: FrameWork utilizado para el Proyecto.
2. **Sqlite**: Motor de base de datos para almacenar y gestionar las preguntas del Quiz.


## Autores

* [mulilla](https://gitlab.com/mulilla)
* [fenix15100](https://gitlab.com/fenix15100)

